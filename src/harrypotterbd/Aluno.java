/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package harrypotterbd;

import java.io.Serializable;

/**
 *
 * @author augus
 */
public class Aluno implements Serializable{
    private int matricula;
    private String nome;
    private int idade;
    private String email;
    private int nivel_magia;
    private String patrono;
    private String casa_nome;
    
    public Aluno(int matricula, String nome, int idade, String email, int nivel_magia, String patrono, String casa_nome){
        this.matricula = matricula;
        this.nome = nome;
        this.idade = idade;
        this.email = email;
        this.nivel_magia = nivel_magia;
        this.patrono = patrono;
        this.casa_nome = casa_nome;
    }

    public int getMatricula() {
        return matricula;
    }

    public void setMatricula(int matricula) {
        this.matricula = matricula;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getIdade() {
        return idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getNivel_magia() {
        return nivel_magia;
    }

    public void setNivel_magia(int nivel_magia) {
        this.nivel_magia = nivel_magia;
    }

    public String getPatrono() {
        return patrono;
    }

    public void setPatrono(String patrono) {
        this.patrono = patrono;
    }

    public String getCasa_nome() {
        return casa_nome;
    }

    public void setCasa_nome(String casa_nome) {
        this.casa_nome = casa_nome;
    }
    
    
}
