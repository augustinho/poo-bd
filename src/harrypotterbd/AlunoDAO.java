/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package harrypotterbd;

import java.sql.Statement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
/**
 *
 * @author aurea
 */
public class AlunoDAO {
    Connection con;
    PreparedStatement pst;
    Statement st;
    ResultSet rs;

    String database = "HARRYPOTTER";
    String url = "jdbc:mysql://localhost:3306/" + database + "?useTimezone=true&serverTimezone=UTC&useSSL=false";
    String user = "root";
    String password = "alaska12";

    boolean sucesso = false;

    public void connectToDb() {
        try {
            con = DriverManager.getConnection(url, user, password);
            System.out.println("Conexão feita com sucesso!");
        } catch (SQLException ex) {
            System.out.println("Erro: " + ex.getMessage());
        }
    }

    
    // * *************************INSERIR DADOS*******************************
     
    public boolean inserirAluno(Aluno novoAluno) {
        connectToDb();
        String sql = "INSERT INTO aluno (matricula, nome, idade, email, nivel_magia, patrono, casa_nome) values (?, ?, ?, ?, ?, ?, ?)";

        try {
            //consulta dinamica (pst)
            pst = con.prepareStatement(sql);
            pst.setInt(1, novoAluno.getMatricula());
            pst.setString(2, novoAluno.getNome());
            pst.setInt(3, novoAluno.getIdade());
            pst.setString(4, novoAluno.getEmail());
            pst.setInt(5, novoAluno.getNivel_magia());
            pst.setString(6, novoAluno.getPatrono());
            pst.setString(7, novoAluno.getCasa_nome());

            pst.execute();
            sucesso = true;
        } catch (SQLException ex) {
            System.out.println("Erro = " + ex.getMessage());
            sucesso = false;
        } finally {
            try {
                con.close();
                pst.close();
            } catch (SQLException ex) {
                System.out.println("Erro = " + ex.getMessage());
            }
        }
        return sucesso;
    }
    
        /***** Atualizar nome do aluno *****/
    
    public boolean atualizarAluno(int matricula, String novoAluno, int idade, String email){
        connectToDb();
        
        //sql
        String sql = "update aluno set nome = ?, idade = ?, email = ? where matricula = ?";
        
        //consulta dinamica (pst)
        try{
            pst = con.prepareStatement(sql);
            pst.setString(1, novoAluno);
            pst.setInt(2, idade );
            pst.setString(3, email);
            pst.setInt(4, matricula);
            pst.execute();
            sucesso = true;
        }
        catch (SQLException ex){
            System.out.println("Erro = " + ex.getMessage());
            sucesso = false;
        }
        finally{
            try{
                con.close();
                pst.close();
            }
            catch(SQLException ex) {
                System.out.println("Erro = " + ex.getMessage());
            }
        }
        return sucesso;
    }
    
    
    
    /***** deletar aluno *****/
    public boolean deletarAluno(int matricula){
        connectToDb();
        
        //sql:
        String sql = "delete from aluno where matricula =?";
        //consulta dinamica (pst)
        try{
            pst = con.prepareStatement(sql);
            pst.setInt(1, matricula);
            pst.execute();
            sucesso = true;
        }
        catch(SQLException ex){
            System.out.println("Erro = " + ex.getMessage());
            sucesso = false;
        }
        finally{
            try{
                con.close();
                pst.close();
            }
            catch(SQLException ex){
                System.out.println("Erro = " + ex.getMessage());
            }
        }
        return sucesso;
    } 
    
    /***** Buscar aluno *****/
    // sem filtro:
    public ArrayList<Aluno> buscarAlunoSemFiltro(){
        ArrayList<Aluno> listaDeAlunos = new ArrayList<>();
        connectToDb();
        //sql:
        String sql = "select * from aluno";
        //n tem parametro, consulta estatica (st)
        try{
            st = con.createStatement();
            rs = st.executeQuery(sql); //como referencia, a tabela resultante da busca
            System.out.println("Lista de alunos: ");
            while(rs.next()){
                //System.out.println(rs.getString("nome"));  <<< isso aqui daria certo se fosse só pra mostrar, n armazena no array.
                Aluno usuarioTemp = new Aluno(rs.getInt("matricula"), rs.getString("nome"), rs.getInt("idade"), rs.getString("email"),rs.getInt("nivel_magia"), rs.getString("patrono"), rs.getString("casa_nome"));
                System.out.println("Matricula: " + usuarioTemp.getMatricula());
                System.out.println("Nome: " + usuarioTemp.getNome());
                System.out.println("Idade: " + usuarioTemp.getIdade());
                System.out.println("Email: " + usuarioTemp.getEmail());
                System.out.println("Nivel magia: " + usuarioTemp.getNivel_magia());
                System.out.println("Patrono: " + usuarioTemp.getPatrono());
                System.out.println("Casa: " + usuarioTemp.getCasa_nome());
                System.out.println("------------------------------------");
                listaDeAlunos.add(usuarioTemp);
            }
            sucesso = true;
        }
        catch(SQLException ex){
            System.out.println("Erro = " + ex.getMessage());
            sucesso = false;
        }
        finally{
            try{
                con.close();
                st.close();
            }
            catch(SQLException ex){
                System.out.println("Erro = " + ex.getMessage());
            }
        }
         return listaDeAlunos;      
    }

    public void deletarAluno(String matricula) {
        connectToDb();
        
        //sql:
        String sql = "delete from aluno where matricula =?";
        //consulta dinamica (pst)
        try{
            pst = con.prepareStatement(sql);
            pst.setString(1, matricula);
            pst.execute();
            sucesso = true;
        }
        catch(SQLException ex){
            System.out.println("Erro = " + ex.getMessage());
            sucesso = false;
        }
        finally{
            try{
                con.close();
                pst.close();
            }
            catch(SQLException ex){
                System.out.println("Erro = " + ex.getMessage());
            }
        }
        
    } 
    public ArrayList<Aluno> listarAluno(){
        ArrayList<Aluno> listando = new ArrayList<>();
        connectToDb();
        String sql = "select * from aluno";
        //n tem parametro, consulta estatica (st)
         try{
            st = con.createStatement();
            rs = st.executeQuery(sql);
            System.out.println("Lista de alunos: ");
            while(rs.next()){
                Aluno usuarioTemp = new Aluno(rs.getInt("matricula"), rs.getString("nome"), rs.getInt("idade"), rs.getString("email"),rs.getInt("nivel_magia"), rs.getString("patrono"), rs.getString("casa_nome"));
                listando.add(usuarioTemp);
            }
        }
         catch(SQLException ex){
            System.out.println("Erro = " + ex.getMessage());
            sucesso = false;
        }
        finally{
            try{
                con.close();
                st.close();
            }
            catch(SQLException ex){
                System.out.println("Erro = " + ex.getMessage());
            }
        }
         return listando;
    }
}
