/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package harrypotterbd.Tela;

import harrypotterbd.Aluno;
import harrypotterbd.AlunoDAO;
import harrypotterbd.Arquivo;
import java.util.ArrayList;

/**
 *
 * @author aurea
 */
public class Lista extends javax.swing.JFrame {

    Arquivo arquivo = new Arquivo();

    /**
     * Creates new form Lista
     */
    public Lista() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jLista_Nomes = new javax.swing.JList();
        jScrollPane2 = new javax.swing.JScrollPane();
        jLista_email = new javax.swing.JList();
        btn_listar = new javax.swing.JButton();
        btn_voltarMenu = new javax.swing.JButton();
        lbl_nome = new javax.swing.JLabel();
        lbl_email = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        jLista_casa = new javax.swing.JList();
        lbl_idade = new javax.swing.JLabel();
        jScrollPane4 = new javax.swing.JScrollPane();
        jLista_idade = new javax.swing.JList();
        jScrollPane5 = new javax.swing.JScrollPane();
        jLista_matricula = new javax.swing.JList();
        jScrollPane6 = new javax.swing.JScrollPane();
        jLista_patrono = new javax.swing.JList();
        jScrollPane7 = new javax.swing.JScrollPane();
        jLista_nivelmagia = new javax.swing.JList();
        lbl_matricula = new javax.swing.JLabel();
        lbl_patrono = new javax.swing.JLabel();
        lbl_nivelmagia = new javax.swing.JLabel();
        lbl_casa = new javax.swing.JLabel();
        lbl_fundo = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jScrollPane1.setViewportView(jLista_Nomes);

        jPanel1.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 50, 110, 300));

        jScrollPane2.setViewportView(jLista_email);

        jPanel1.add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 50, 110, 300));

        btn_listar.setText("LISTAR");
        btn_listar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_listarActionPerformed(evt);
            }
        });
        jPanel1.add(btn_listar, new org.netbeans.lib.awtextra.AbsoluteConstraints(650, 380, -1, -1));

        btn_voltarMenu.setText("VOLTAR");
        btn_voltarMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_voltarMenuActionPerformed(evt);
            }
        });
        jPanel1.add(btn_voltarMenu, new org.netbeans.lib.awtextra.AbsoluteConstraints(750, 380, -1, -1));

        lbl_nome.setForeground(new java.awt.Color(255, 255, 255));
        lbl_nome.setText("NOME DOS ALUNOS");
        jPanel1.add(lbl_nome, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 20, -1, -1));

        lbl_email.setForeground(new java.awt.Color(255, 255, 255));
        lbl_email.setText("EMAIL");
        jPanel1.add(lbl_email, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 20, -1, -1));

        jScrollPane3.setViewportView(jLista_casa);

        jPanel1.add(jScrollPane3, new org.netbeans.lib.awtextra.AbsoluteConstraints(730, 50, 110, 300));

        lbl_idade.setForeground(new java.awt.Color(255, 255, 255));
        lbl_idade.setText("IDADE");
        jPanel1.add(lbl_idade, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 20, -1, -1));

        jScrollPane4.setViewportView(jLista_idade);

        jPanel1.add(jScrollPane4, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 50, 110, 300));

        jScrollPane5.setViewportView(jLista_matricula);

        jPanel1.add(jScrollPane5, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 50, 110, 300));

        jScrollPane6.setViewportView(jLista_patrono);

        jPanel1.add(jScrollPane6, new org.netbeans.lib.awtextra.AbsoluteConstraints(490, 50, 110, 300));

        jScrollPane7.setViewportView(jLista_nivelmagia);

        jPanel1.add(jScrollPane7, new org.netbeans.lib.awtextra.AbsoluteConstraints(610, 50, 110, 300));

        lbl_matricula.setForeground(new java.awt.Color(255, 255, 255));
        lbl_matricula.setText("MATRICULA");
        jPanel1.add(lbl_matricula, new org.netbeans.lib.awtextra.AbsoluteConstraints(404, 20, -1, -1));

        lbl_patrono.setForeground(new java.awt.Color(255, 255, 255));
        lbl_patrono.setText("PATRONO");
        jPanel1.add(lbl_patrono, new org.netbeans.lib.awtextra.AbsoluteConstraints(521, 20, -1, -1));

        lbl_nivelmagia.setForeground(new java.awt.Color(255, 255, 255));
        lbl_nivelmagia.setText("NÍVEL DE MAGIA");
        jPanel1.add(lbl_nivelmagia, new org.netbeans.lib.awtextra.AbsoluteConstraints(630, 20, -1, -1));

        lbl_casa.setForeground(new java.awt.Color(255, 255, 255));
        lbl_casa.setText("CASA DE HOGWARTS");
        jPanel1.add(lbl_casa, new org.netbeans.lib.awtextra.AbsoluteConstraints(736, 20, -1, -1));

        lbl_fundo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/harrypotterbd/Imagem/pottermore-patronus-test.jpg"))); // NOI18N
        jPanel1.add(lbl_fundo, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, -30, 860, 440));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents



    private void btn_listarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_listarActionPerformed
       listarAlunosBD();
    }//GEN-LAST:event_btn_listarActionPerformed

    private void chamarTelaMenu() {
        new Menu().setVisible(true);
        dispose();
    }
    private void btn_voltarMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_voltarMenuActionPerformed
        // TODO add your handling code here:
        chamarTelaMenu();
    }//GEN-LAST:event_btn_voltarMenuActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Lista.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Lista.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Lista.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Lista.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Lista().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_listar;
    private javax.swing.JButton btn_voltarMenu;
    private javax.swing.JList jLista_Nomes;
    private javax.swing.JList jLista_casa;
    private javax.swing.JList jLista_email;
    private javax.swing.JList jLista_idade;
    private javax.swing.JList jLista_matricula;
    private javax.swing.JList jLista_nivelmagia;
    private javax.swing.JList jLista_patrono;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JLabel lbl_casa;
    private javax.swing.JLabel lbl_email;
    private javax.swing.JLabel lbl_fundo;
    private javax.swing.JLabel lbl_idade;
    private javax.swing.JLabel lbl_matricula;
    private javax.swing.JLabel lbl_nivelmagia;
    private javax.swing.JLabel lbl_nome;
    private javax.swing.JLabel lbl_patrono;
    // End of variables declaration//GEN-END:variables

    public void listarAlunosBD() {

        // Armazenado os alunos do banco
        ArrayList<Aluno> classe = new ArrayList<>();

        // Puxar os dados do banco
        AlunoDAO adao = new AlunoDAO();

        // Pegando os dados do banco
        classe = adao.buscarAlunoSemFiltro();

        // Exibir os dados
        String[] nome = new String[classe.size()];
        String[] matricula = new String[ classe.size()];
        String[] email = new String[classe.size()];
        String[] idade = new String[classe.size()];      
        String[] patrono = new String[classe.size()];
        String[] nivel_magia = new String[classe.size()];
        String[] casa = new String[classe.size()];
        
        for (int i = 0; i < nome.length; i++) {
            nome[i] = classe.get(i).getNome();
        }
        
        for (int i = 0; i < matricula.length; i++) {
            // converter int para string
            matricula[i] = classe.get(i).getMatricula() + "";
        }
        for (int i = 0; i < email.length; i++) {

            email[i] = classe.get(i).getEmail();

        }
        for (int i = 0; i < idade.length; i++) {           
            idade[i] = Integer.toString(classe.get(i).getIdade());

        }
        
        for (int i = 0; i < patrono.length; i++) {

            patrono[i] = classe.get(i).getPatrono();

        }
        for (int i = 0; i < nivel_magia.length; i++) {

            nivel_magia[i] = Integer.toString(classe.get(i).getNivel_magia());

        }
        for (int i = 0; i < casa.length; i++) {

            casa[i] = classe.get(i).getCasa_nome();

        }
        
        jLista_Nomes.setListData(nome);
        jLista_matricula.setListData(matricula);
        jLista_email.setListData(email);
        jLista_idade.setListData(idade); 
        jLista_patrono.setListData(patrono);
        jLista_nivelmagia.setListData(nivel_magia);
        jLista_casa.setListData(casa);
    }
}
